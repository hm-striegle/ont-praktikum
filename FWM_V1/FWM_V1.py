##!/usr/bin/env python2
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_fiber import pypho_fiber
from pypho_cwlaser import pypho_cwlaser
from pypho_optfi import pypho_optfi
from pypho_functions import *
import numpy as np
import copy
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt


plt.close('all')

# Define network elements
gp       = pypho_setup(nos = 16, sps = 256, symbolrate = 10e9)
sig_f0   = pypho_cwlaser(glova = gp, power = 0, Df = 0,  teta =0*np.pi/4.0)
sig_f1   = pypho_cwlaser(glova = gp, power = 0, Df = 20, teta = 0)
filter_f0 = pypho_optfi(glova = gp, Df = 0, B = 150)
SSMF     = pypho_fiber(glova = gp, l = 20e3,  D = 16.00,   S = 0.0, alpha = 0.2, gamma = 1.4, phi_max = .001)

# Simulation

# Define wavelength channels

E_f0    = sig_f0()                                              
E_f1    = sig_f1()  
E       = copy.deepcopy(E_f1)

E[0]['E'][0] = E_f0[0]['E'][0] + E_f1[0]['E'][0]  # Multiplex all signals X-Pol
E[0]['E'][1] = E_f0[0]['E'][1] + E_f1[0]['E'][1]  # Multiplex all signals Y-Pol 


E_Tx = copy.deepcopy(E)

# Plot Spectrum Input signals
plt.figure(1)
ax1 = plt.subplot(2, 1, 1)
plt.semilogy((gp.freqax()-gp.f0)*1e-9, (abs(fftshift(fft(E[0]['E'][0]   )**2))), 'r', label='X-Pol')

plt.title("Input spectrum", loc='left')
plt.ylabel('Spec. density');
ax_tmp = np.abs(gp.freqax()[0]-gp.f0)*1e-9//(np.abs(sig_f1.Df[0]-sig_f0.Df[0]))
ax_tmp *=  np.abs(sig_f1.Df[0]-sig_f0.Df[0])
plt.xticks(np.arange( -ax_tmp, ax_tmp, np.abs(sig_f1.Df[0]-sig_f0.Df[0])))
plt.grid(True)
plt.legend()

# Fiber transmission
E = SSMF(E = E)    


# Plot Spectrum Output signals
ax2 = plt.subplot(2, 1, 2, sharex=ax1)
plt.semilogy((gp.freqax()-gp.f0)*1e-9, (abs(fftshift(fft(E[0]['E'][0]   )**2))), 'r', label='X-Pol')
plt.title("Output spectrum", loc='left')
plt.ylabel('Spec. density'); plt.xlabel('Frequency deviation [GHz]');
plt.grid(True)
plt.show()
