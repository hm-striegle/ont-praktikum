##!/usr/bin/env python2
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_functions import *
from pypho_optfi_V3 import pypho_optfi
from pypho_arbmod import pypho_arbmod
import numpy as np
import copy
import matplotlib.pyplot as plt
from scipy import special
from numpy.random import standard_normal
import time

###############################################################################

#### HIER Anzahl der Konstellationspunkte definieren ###
# M = 4     : 4QAM (4PSK)
# M = 16    : 16QAM
# M = 64    : 64QAM
# M = 256   : 256QAM
M = 4

### Hier Eb/N0 in dB definieren ###
EbN0 = 10       # in dB 

### Hier die Anzahl der Monte-Carlo-Versuche definieren ###
N_MC = 10000


###############################################################################


plt.close("all")

 # Plot theoretical BER curves
EN = 10**( np.arange(0,20,1)/10)
plt.figure(100)
c=0;colco = ['r', 'g', 'b', 'm']
for Mloop in np.array([4, 16, 64, 256]):
    plt.semilogy(10*np.log10(EN), 2/np.log2(Mloop) * (1 - 1/np.sqrt(Mloop)) * special.erfc(np.sqrt(3/2*np.log2(Mloop)/(Mloop-1) * EN)), '-'+colco[c], label=str(Mloop)+'QAM BER', linewidth=1, alpha=0.5)       
    plt.semilogy(10*np.log10(EN),  1 - (1 - (1 - 1/np.sqrt(Mloop))  * special.erfc(np.sqrt(3/2*np.log2(Mloop)/(Mloop-1) * EN)))**2, '-.'+colco[c], label=str(Mloop)+'QAM SER', linewidth=1, alpha=0.5)  
    c += 1
    
plt.legend();plt.ylim([10e-8,1])
plt.minorticks_on();
plt.grid(which='major', linestyle='-', linewidth='0.5', color='black')
plt.grid(which='minor', linestyle='-', linewidth='0.5', color='grey')
plt.title("SER und BER");plt.xlabel( r'$\frac{E_b}{N_0}$');plt.ylabel('SER, BER')

# Define network elements
gp       = pypho_setup(nos = 1*128, sps = 1*128, symbolrate = 10e6)
symsrc   = pypho_symbols(glova = gp, nos = gp.nos, pattern = 'random')
esigsrc  = pypho_signalsrc(glova = gp, pulseshape = 'rect' )
sig_1550 = pypho_lasmod(glova = gp, power = 0, Df = 0, teta = np.pi/4.0)
modulator= pypho_arbmod(glova = gp) 
cosr_filter   = pypho_optfi(glova = gp, Df = 0, B = 10.0e6, filtype = 'cosrolloff_inv_sinc', alpha = 1.0, loss = 0.0)
 


# Konstellationspunkte und Gray-Kodierung definieren
symbols_x = symsrc(p1=M)
if (M==1) or (np.mod(np.log2(M),2)!=0): # M not a even power of 2
    raise ValueError('Only square MQAM supported. M must be even power of 2')

n = np.arange(0,M) # Sequential address from 0 to M-1 (1xM dimension)
a = np.asarray([x ^ (x>>1) for x in n]) #convert linear addresses to Gray code
D = np.sqrt(M).astype(int) #Dimension of K-Map - N x N matrix
a = np.reshape(a,(D,D)) # NxN gray coded matrix
oddRows=np.arange(start = 1, stop = D ,step=2) # identify alternate rows
nGray = np.reshape(a,(M)) # reshape to 1xM - Gray code walk on KMap
#Construction of ideal M-QAM constellation from sqrt(M)-PAM
(x,y) = np.divmod(nGray,D) #element-wise quotient and remainder
Ax=2*x+1-D # PAM Amplitudes 2d+1-D - real axis
Ay=2*y+1-D # PAM Amplitudes 2d+1-D - imag axis
constellation = Ax+1j*Ay
constellation /= np.max(np.abs(constellation))*0.5*np.sqrt(2)
constpts = [(      constellation ),
                 (      [(0),(0)], [(0),(1)], [(1),(1)], [(1),(0)]             )]   

# Create signal
onebits = symsrc(pattern = 'ones')
esig = esigsrc(bitsequence = onebits)
E_Tx = sig_1550(esig = esig)   
E = modulator( E = E_Tx, constpoints = [constpts, constpts], symbols = [symbols_x, symbols_x] )          # Modulate
E = E[0]['E'][0]
E /= np.max(np.abs(E))*0.5*np.sqrt(2)
E_Tx = copy.deepcopy(E)


for EbN0 in np.arange(3, 12, 2):
    
    # Rauschen definieren
    EbN0_lin= 10**(EbN0/10)
    Eb      = np.mean(np.abs(E)**2) /(gp.symbolrate*np.log2(M))
    N0      = Eb / EbN0_lin
    P_N_band = N0 * gp.frange
    N_bERR = 0;N_sERR = 0;BER_f = 0;SER_f = 0
    
    print(str("n") + "\t\t"+str("N_ERRs") +"\t" + str("SER") + "\t\t\t" + str("N_ERRb") + "\t"  + str("BER"))
    print("---------------------------------------------")
    plt.figure(1); plt.grid(True)
    for n_MC in range(N_MC):
        
        E = copy.deepcopy(E_Tx)
        
        # Generate an sample of white noise     
        
        noise_re = np.sqrt(P_N_band) * np.random.normal(0, np.sqrt(2)/2, len(E))
        noise_im = np.sqrt(P_N_band) * np.random.normal(0, np.sqrt(2)/2, len(E))
        
        E += noise_re + 1.j*noise_im
        
        E = cosr_filter(E = E, plotfilter = False) 
    
            
        E_samp = E[gp.sps//2::gp.sps]
    
        # Plot some samples in the constellation diagramme
        
            
        # Calculate BER & SER        
        a = np.reshape(E_samp, (-1, 1))
        c= np.argmin(np.abs(np.subtract(a, constellation)), axis=1)
        N_sERR += np.size(np.where(symbols_x != c)) 
        i = nGray[c] ^ nGray[symbols_x]
        N_bERR += np.sum([bin(x).count("1") for x in i])
    
    
        if np.mod(n_MC,100) == 0:        
            print(str(n_MC).zfill(5) +"\t"+str(N_sERR).zfill(6)+"\t" +  '{:.2e}'.format(N_sERR/((1+n_MC)*gp.nos)) + "\t"+ str(N_bERR).zfill(6)+ "\t"+ '{:.2e}'.format(N_bERR/((1+n_MC)*gp.nos*np.log2(M))) + "\t" )
    
        if 300 > n_MC:            
            plt.plot(np.real(E_samp), np.imag(E_samp), 'g.', markersize=1, alpha=0.3)
            plt.plot(np.real(E_samp[np.where(symbols_x != c)]), np.imag(E_samp[np.where(symbols_x != c)]), 'r.', markersize=1, alpha=0.3)
    
    
    
    
    
    
    plt.figure(100)
    plt.semilogy(EbN0, N_bERR/(N_MC*gp.nos*np.log2(M)), 'ko')
    plt.semilogy(EbN0, N_sERR/(N_MC*gp.nos), 'kd')
    
    # Plot conestallation diagrammes  
    plt.figure(1)    
    for c in range(int(np.sqrt(M)-1)):
        plt.plot([-1.5, 1.5], (-1 + 2/(np.sqrt(M)-1)*(0.5+c))*np.array([1.0, 1.0]), "grey", linewidth=1)
        plt.plot((-1+2/(np.sqrt(M)-1)*(0.5+c))*np.array([1.0, 1.0]), [-1.5, 1.5], "grey", linewidth=1)
    
      
    plt.plot(np.real(constellation),np.imag(constellation),'bo')
    
    for c in range(int(np.sqrt(M)-1)):
        plt.plot([-1.5, 1.5], (-1 + 2/(np.sqrt(M)-1)*(0.5+c))*np.array([1.0, 1.0]), "grey")
        plt.plot((-1+2/(np.sqrt(M)-1)*(0.5+c))*np.array([1.0, 1.0]), [-1.5, 1.5], "grey")
        
    for c in range(M):
        plt.text(np.real(constellation[c]), np.imag(constellation[c]), str(format(nGray[c], '#010b'))[-int(np.sqrt(M)):])
    
    plt.grid(False)

