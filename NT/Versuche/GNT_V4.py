##!/usr/bin/env python2
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_functions import *
from pypho_optfi import pypho_optfi
from pypho_arbmod import pypho_arbmod
import numpy as np
import copy
import matplotlib.pyplot as plt
from scipy import special
from numpy.random import standard_normal
from PIL import Image, ImageOps




###############################################################################


### Hier Eb/N0 in dB definieren ###
EbN0 = 10       # in dB 

## Hier Filename vom Testbild definieren (1-3)
im = Image.open('testbild2.png') # Can be many different formats.
im_0 = copy.deepcopy(im)


###############################################################################


M = 256
if (M==1) or (np.mod(np.log2(M),2)!=0): # M not a even power of 2
    raise ValueError('Only square MQAM supported. M must be even power of 2')

n = np.arange(0,M) # Sequential address from 0 to M-1 (1xM dimension)
a = np.asarray([x ^ (x>>1) for x in n]) #convert linear addresses to Gray code
D = np.sqrt(M).astype(int) #Dimension of K-Map - N x N matrix
a = np.reshape(a,(D,D)) # NxN gray coded matrix
oddRows=np.arange(start = 1, stop = D ,step=2) # identify alternate rows

nGray = np.reshape(a,(M)) # reshape to 1xM - Gray code walk on KMap
#nGray = np.arange(0,256, dtype=int)
#Construction of ideal M-QAM constellation from sqrt(M)-PAM
(x,y) = np.divmod(nGray,D) #element-wise quotient and remainder
Ax=2*x+1-D # PAM Amplitudes 2d+1-D - real axis
Ay=2*y+1-D # PAM Amplitudes 2d+1-D - imag axis
constellation = Ax+1j*Ay

#nGray_tmp1 = nGray[1]
#nGray[1] = nGray[170]
#nGray[170] = nGray_tmp1

#nGray = np.arange(0,256, dtype=int)
#nGray = np.roll(nGray, M//2)
constellation /= np.max(np.abs(constellation))
plt.plot(np.real(constellation),np.imag(constellation),'.')
#sys.exit()


plt.close("all")

# Define network elements
gp       = pypho_setup(nos = 2**14, sps = 1*32, symbolrate = 10e6)
symsrc   = pypho_symbols(glova = gp, nos = gp.nos, pattern = 'debruijn')
filter_cor = pypho_optfi(glova = gp, Df = 0, B = 0.25*gp.symbolrate, filtype = 'cosrolloff_inv_sinc', alpha = 1.0, loss = 0.0) # gauss, cosrolloff_inv_sinc
esigsrc  = pypho_signalsrc(glova = gp, pulseshape = 'rect' )
sig_1550 = pypho_lasmod(glova = gp, power = 0, Df = 0, teta = np.pi/4.0)
modulator= pypho_arbmod(glova = gp) 


constpts = [(     constellation ),
                 (  [(0),(0),(0)], [(0),(0),(1)], [(0),(1),(0)], [(0),(1),(1)], [(1),(1),(1)],
                  [(1),(1),(0)], [(1),(0),(1)], [(1),(0),(0)])] 
 

# Create symbolpattern
symbols_x = symsrc(p1=2**14)

c = 0
for pix in im.getdata():
    symbols_x[c] = np.where(nGray == pix[0])[0][0]
    c += 1


# Create signal
onebits = symsrc(pattern = 'ones')
esig = esigsrc(bitsequence = onebits)
E_Tx = sig_1550(esig = esig)   
#E_Tx[0]['E'][0] = E_Tx[0]['E'][0] * 0 + 1
E = modulator( E = E_Tx, constpoints = [constpts, constpts], symbols = [symbols_x, symbols_x] )          # Modulate
E = E[0]['E'][0]
E /= np.max(np.abs(E))


EbN0    = 10**(EbN0/10)
Eb      = np.sum(np.abs(E)**2 * gp.timeax()[1] ) / (np.log2(M)*gp.nos)
N0      = Eb / EbN0
P_N_band = N0 * gp.frange

# Generate an sample of white noise        
noise_re = np.random.normal(0, np.sqrt(P_N_band/2), len(E))
noise_im = np.random.normal(0, np.sqrt(P_N_band/2), len(E))
E += noise_re + 1.j*noise_im
E = filter_cor(E = E)

plot_signal = False
if plot_signal:
    plt.figure(1)
    plt.plot(np.rot90(E.reshape((gp.nos, gp.sps))), 'g')    
    plt.title("Augendiagramm")
    plt.xlabel('Time $t$ in ps')
    plt.ylabel('Voltage  $u(t)$ in V')
    plt.grid(True)
    
    plt.figure(2)
    #plt.plot(gp.timeax(), E/np.max(E), 'g')
    plt.plot(np.real(E[gp.sps//2::gp.sps]), np.imag(E[gp.sps//2::gp.sps]), 'go')
    plt.plot(np.real(constellation), np.imag(constellation), 'r.')
    plt.title("Konstellationsdiagramm")
    plt.grid(True)
    
    

# Calculate BER

E_samp = E[gp.sps//2::gp.sps]
N_ERR = 0

for c, samp in enumerate(E_samp):
    
    newpix = np.argmin(np.abs(samp - constellation))
    im.putpixel((c%128, c//128), (nGray[newpix], nGray[newpix], nGray[newpix]))

    i = nGray[newpix] ^ nGray[symbols_x[c]]
    N_ERR += bin(i).count("1")
    if plot_signal and bin(i).count("1")!= 0:
        plt.plot(np.real(samp), np.imag(samp), 'm*')        

    #im.putpixel((c%128, c//128), (nGray[symbols_x[c]], nGray[symbols_x[c]], nGray[symbols_x[c]]))

    
img = im.resize((1024, 1024), resample=Image.BOX)
img_0 = im_0.resize((1024, 1024), resample=Image.BOX)
#img.show()

fig, axs = plt.subplots(1,2)
axs[0].imshow(im_0)
plt.grid(False)
axs[1].imshow(im)
plt.grid(False)
plt.show()
print("BER = ", np.log10(N_ERR/(128*128*8)))
