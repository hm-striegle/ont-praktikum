##!/usr/bin/env python2
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_functions import *
from pypho_optfi import *
import numpy as np
import copy
import matplotlib.pyplot as plt

plt.close("all")

# Setup, Definitionen
# Hier nichts verändern!
gp          = pypho_setup(nos = 2*4096, sps = 1*256, symbolrate = 10e6)
bitsrc      = pypho_symbols(glova = gp, nos = gp.nos, pattern = 'random')
pulsesrc    = pypho_signalsrc(glova = gp, pulseshape = 'gauss_rz' , fwhm = 0.01)
mod         = pypho_lasmod(glova = gp, power = 0, Df = 0, teta = 0.25*np.pi)
Nq_filter   = pypho_optfi(glova = gp, Df = 0, B = 8.27e6, filtype = 'cosrolloff', alpha = 0.0, loss = 0.0)



# Simulation
bits    = bitsrc()                      # Symbole erzeugen
esig    = pulsesrc(bitsequence = bits)  # Signal erzeugen 1
E       = mod(esig = esig)              # Signal erzeugen 2
E       = np.abs(E[0]['E'][0])          # Signal anpassen
E       /= np.max(E)                    # Normieren 


# Signal vor dem Filter darstellen
plt.figure(1); plt.plot(gp.timeax(), E, 'r')
spek = np.abs(fftshift(fft(E))); spek /= np.max(np.abs(fftshift(fft(E))))
plt.figure(100); plt.plot((gp.freqax()-gp.f0)/gp.symbolrate, spek, 'r', label='Signal')


# Optimale Bandbreite bestimmen und variieren
# alpha Faktor verändern und Augenöffnung in x-Richtung bestimmen
E = Nq_filter(E = E, B = 8.27e6, filtype = 'cosrolloff', alpha = 1.0, plotfilter = True)


# Signal nach dem Filter darstellen
# Zeitbereichssignal
plt.figure(1)
plt.plot(gp.timeax(), E/np.max(E), 'g')
plt.plot(gp.timeax()[gp.sps//2::gp.sps], E[gp.sps//2::gp.sps]/np.max(E), 'go')
plt.xlabel('Time $t$ in ps')
plt.ylabel('Voltage  $u(t)$ in V')
plt.title("Signal im Zeitbereich")
plt.grid(True)


# Spektrum
plt.figure(100)
spek = np.abs(fftshift(fft(E))); spek /= np.max(np.abs(fftshift(fft(E))))
#plt.plot((gp.freqax()-gp.f0)/gp.symbolrate, spek, 'g')
plt.title("Signalspektrum")
plt.xlabel('Frequenz $f / T_s$ ')
plt.ylabel('Spektrale Leistungsdichte  $\partial u(f)/ \partial f$')
plt.xlim(-5, 5)
plt.grid(True)
plt.legend()

# Augendiagramm
plt.figure(2)
E -= np.mean(np.abs(E[gp.sps//2::gp.sps]))
E  /= (np.max(np.abs(E[gp.sps//2::gp.sps])))
plt.plot(gp.timeax()[0:gp.sps], np.rot90(E.reshape((gp.nos, gp.sps))), 'g')
plt.title("Augendiagramm")
plt.xlabel('Time $t$ in ps')
plt.ylabel('Voltage  $u(t)$ in V')
plt.grid(True)