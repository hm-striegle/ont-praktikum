##!/usr/bin/env python2
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_functions import *
from pypho_optfi import *
import numpy as np
import copy
import matplotlib.pyplot as plt

plt.close("all")

# Setup, Definitionen
# Hier nichts verändern!
gp          = pypho_setup(nos =8*128, sps = 16*128, symbolrate = 10e6)
bitsrc      = pypho_symbols(glova = gp, nos = gp.nos, pattern = 'singlepulse')
mod         = pypho_lasmod(glova = gp, power = 0, Df = 0, teta = 0.0*np.pi)
H_filter    = pypho_optfi(glova = gp, Df = 0, B = 10.00e6, filtype = 'cosrolloff', alpha = 0.99, loss = 0.0)



# Pulsform ändern
pulsesrc  = pypho_signalsrc(glova = gp, pulseshape = 'gauss_rz' , fwhm = 0.0001) # Dirac
#pulsesrc    = pypho_signalsrc(glova = gp, pulseshape = 'rect')      # Rechteck


# Simulation
bits    = bitsrc()                      # Symbole erzeugen
esig    = pulsesrc(bitsequence = bits)  # Signal erzeugen 1
E       = mod(esig = esig)              # Signal erzeugen 2
E       = np.abs(E[0]['E'][0])          # Signal anpassen
E       /= np.max(E)                    # Normieren 

# Signal vor dem Filter darstellen
t_start = gp.sps*(gp.nos//2-6); t_ende = gp.sps*(gp.nos//2+7)
plt.figure(1); plt.plot(gp.timeax()[t_start:t_ende], E[t_start:t_ende], 'r')
spek = np.abs(fftshift(fft(E))); spek /= np.max(np.abs(fftshift(fft(E))))
plt.figure(100); plt.plot((gp.freqax()-gp.f0)/gp.symbolrate, spek, 'r', linewidth=3, label="Eingangssignal")







# Optimale Bandbreite bestimmen und variieren

# 1: Nur cos-roll-off-Filter
E = H_filter(E = E, B = 10.00e6, filtype = 'cosrolloff', alpha = 0.5,  plotfilter = True)

# 2: x/sin(x)-Filter
#E = H_filter(E = E, B = 10.00e6, filtype = 'inv_sinc', alpha = 0.5,  plotfilter = True)

# 3: cosrolloff und x/sin(x)-Filter
#E = H_filter(E = E, B = 10.00e6, filtype = 'cosrolloff_inv_sinc', alpha = 0.5,  plotfilter = True)








# Signal nach dem Filter darstellen
plt.figure(1)
plt.plot(gp.timeax()[t_start:t_ende], E[t_start:t_ende]/np.max(E[t_start:t_ende]), 'g')
plt.plot(gp.timeax()[t_start+gp.sps//2:t_ende:gp.sps], E[t_start+gp.sps//2:t_ende:gp.sps]/np.max(E[t_start:t_ende]), 'go')
plt.xlabel('Time $t$ in ps')
plt.ylabel('Voltage  $u(t)$ in V')
plt.title("Signal im Zeitbereich")
plt.grid(True)


# Spektrum
plt.figure(100)
spek = np.abs(fftshift(fft(E))); spek /= np.max(np.abs(fftshift(fft(E))))
plt.plot((gp.freqax()-gp.f0)/gp.symbolrate, spek, 'g', label="Ausgangssignal")
plt.title("Signalspektrum")
plt.xlabel('Frequenz $f / T_s$ ')
plt.ylabel('Spektrale Leistungsdichte  $\partial u(f)/ \partial f$')
plt.xlim(-5, 5)
plt.legend()
plt.grid(True)

# Augendiagramm
'''
plt.figure(2)
E -= np.mean(E) 
E  /= np.max(E)
plt.plot(gp.timeax()[0:gp.sps], np.rot90(E.reshape((gp.nos, gp.sps))), 'g')
plt.title("Augendiagramm")
plt.xlabel('Time $t$ in ps')
plt.ylabel('Voltage  $u(t)$ in V')
plt.grid(True)
'''