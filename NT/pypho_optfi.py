#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pypho.py
#  
#  Copyright 2014 Arne Striegler (arne.striegler@hm.edu)
#  
#   
#  
# Simulations direct modulated laser source
# Without Chirp
#
########################################################################
import matplotlib.pyplot as plt
import numpy as np
import sys
from pypho_functions import *
import pyfftw

########################################################################

class pypho_optfi(object):
    def __init__(self, glova = None, Df = None, B = None, filtype = None, alpha = None, loss = None, plotfilter = False):

        if glova == None:            
            print ("ERROR: pypho_optfi: You must define the global variables")
            sys.exit("PyPho stopped!")
            
        self.glova      = glova
        self.Df         = None
        self.B          = None
        self.filtype    = None
        self.alpha      = None
        self.loss       = None
        self.plotfilter = None        
        self.set(Df, B, filtype, alpha, loss , plotfilter)                

      
        
            
########################################################################

    def __call__(self,  E = None, Df = None, B = None, filtype = None, alpha = None, loss = None, plotfilter = False):

        n_samples = self.glova.sps * self.glova.nos


        
        self.set(Df, B, filtype, alpha, loss, plotfilter)    

        
        if self.filtype == "gaussrolloff":        
            z=0
            filfunc=np.zeros(self.glova.nos*self.glova.sps)
            res = (self.glova.symbolrate/self.glova.nos)
            offset = -self.Df*1e9-(self.glova.sps*self.glova.nos/2)*(self.glova.symbolrate/self.glova.nos)


            while z<(self.glova.nos*self.glova.sps):
                if(abs(z*res+offset)<=((1-self.alpha)*self.B/2*1e9)):
                    filfunc[z]=1
                    
                elif(0<z*res+offset-(1-self.alpha)*self.B/2*1e9):
                    filfunc[z]=np.exp(-(((z*res+offset)-(1-self.alpha)*self.B/2*1e9)*((np.sqrt(np.abs(np.log(0.5)))/(self.alpha*self.B/2*1e9))))**2)
            
                elif(0>z*res+offset+(1-self.alpha)*self.B/2*1e9):
                    filfunc[z]=np.exp(-(((z*res+offset)+(1-self.alpha)*self.B/2*1e9)*((np.sqrt(np.abs(np.log(0.5)))/(self.alpha*self.B/2*1e9))))**2)
                
                z=z+1        
        
        
            z = 0
            
        
        
        
        if self.filtype == "gauss":
            
            filfunc = np.exp(-( ( self.glova.freqax() + ( - self.glova.f0 - self.Df*1e9 ) )/ (self.B*2.0e9 / 2.3582))**2 / 2.0 )**2
            
            
            
        if self.filtype == "cosrolloff_inv_sinc" :
            
            T1      = 1.0 / (self.B)

            offset  = -self.Df*1.0e9-(self.glova.sps*self.glova.nos/2.0)*(self.glova.symbolrate/self.glova.nos)

            z = 0
            
            filfunc = np.zeros(self.glova.nos*self.glova.sps)

            while z<(self.glova.nos*self.glova.sps):
                if (abs(z*self.glova.fres+offset)<=((1.0-self.alpha)/(2*T1))):
                    filfunc[z]=1.0
                elif (((1-self.alpha)/(2.0*T1)<abs(z*self.glova.fres+offset)) and (abs(z*self.glova.fres+offset))<=(1+self.alpha)/(2*T1)):
                    #filfunc[z]=(np.cos((np.pi*T1)/(2.0*self.alpha)*(abs(z*self.glova.fres+offset)-((1-self.alpha)/(2*T1))))**2)
                    filfunc[z]= np.cos(T1/(4.0*self.alpha)*(2*np.pi*abs(z*self.glova.fres+offset)+np.pi/T1*(self.alpha-1)))**2
                else:
                    filfunc[z]= 0.0

                z += 1            
            
            filfunc *= 1/np.sinc((self.glova.freqax()-self.glova.f0) * 1/self.B)

            

        if self.filtype == "inv_sinc" :            
               
            filfunc = 1/np.sinc((self.glova.freqax()-self.glova.f0) * 1/self.B)
            
            
            
        if self.filtype == "rect":
                        
            filfunc = np.zeros(self.glova.nos*self.glova.sps)+1e-12
            
            filfunc[ np.where(np.abs(self.glova.freqax()-self.glova.f0 -self.Df*1e9) <= self.B*0.5e9) ] = 1
            


        if self.filtype == "gauss_nth":
    
            filfunc = ( np.exp( -2.0*np.log(2.0) * ( (  - self.glova.freqax() + self.glova.f0 + self.Df*1e9 )  /  (self.B*0.5e9 / (2.0**(self.alpha-1) )**(1.0/float(self.alpha)) ) )**(2*self.alpha) ) )
 


        if self.filtype == "cosrolloff":
            
            T1      = 1.0 / (self.B)
            
            offset  = -self.Df*1.0e9-(self.glova.sps*self.glova.nos/2.0)*(self.glova.symbolrate/self.glova.nos)
            
            z = 0
            
            filfunc = np.zeros(self.glova.nos*self.glova.sps)
            
            while z<(self.glova.nos*self.glova.sps):
                if (abs(z*self.glova.fres+offset)<=((1.0-self.alpha)/(2*T1))):
                    filfunc[z]=1.0
                elif (((1-self.alpha)/(2.0*T1)<abs(z*self.glova.fres+offset)) and (abs(z*self.glova.fres+offset))<=(1+self.alpha)/(2*T1)):
                    #filfunc[z]=(np.cos((np.pi*T1)/(2.0*self.alpha)*(abs(z*self.glova.fres+offset)-((1-self.alpha)/(2*T1))))**2)
                    filfunc[z]= np.cos(T1/(4.0*self.alpha)*(2*np.pi*abs(z*self.glova.fres+offset)+np.pi/T1*(self.alpha-1)))**2
                else:
                    filfunc[z]= 0.0

                z += 1
        
        #E_in = copy.deepcopy(E)
        E   = ifft( fft(E)  *  fftshift(filfunc) )
       

        if self.plotfilter:
            plt.figure(100)
            plt.plot((self.glova.freqax()-self.glova.f0)/self.glova.symbolrate, filfunc, label="Filterfunktion")            
            #plt.plot((self.glova.freqax()-self.glova.f0)/self.glova.symbolrate, fftshift(fft(E)), 'r')
            #plt.plot((self.glova.freqax()-self.glova.f0)/self.glova.symbolrate, fftshift(fft(E_in)), 'g')
            plt.xlabel('Frequenz $f / T_s$ ')
            plt.ylabel('$H(f)$')
            plt.title("Filterfunktion")
            #plt.xlim(-2, 2)
            plt.grid(True)            
        
        return E
            

########################################################################

    def set(self, Df = None, B = None, filtype = None, alpha = None, loss = None, plotfilter = None):
        
        
        if filtype == None and self.filtype != None:
            filtype = self.filtype
        
        if filtype == None and self.filtype == None:
            filtype = "gauss"
            print ("WARNING: pypho_optfi: No filter type specified! So I'm using gaussian fiter!")
   
        self.filtype = filtype            
        
        if filtype == "cosrolloff" or filtype == "gaussrolloff" or filtype == "gauss_nth" or filtype == "cosrolloff_inv_sinc":
            
            if alpha == None and self.alpha != None:
                alpha = self.alpha
            if alpha == None and self.alpha == None:
                print ("WARNING: pypho_optfi: Alpha is not specified! So I'm settin it to 0.25!")
                alpha = 0.25
                
            self.alpha = alpha
                
                   
        if Df == None and self.Df != None:            
            Df = self.Df
                    
        if Df == None and self.Df == None:            
            print ("WARNING: pypho_optfi: No deviation of center frequency specified! So I'm using ",self.glova.f0* 1e-12," THz")
            Df = 0
    
        self.Df = Df 
    
            
        if B == None and self.B != None:            
            B = self.B
                    
        if B == None and self.B == None:            
            B = 100.0  
            print ("WARNING: pypho_optfi: No FWHM bandwidth specified! So I'm using ", B, " GHz")    
                    
        self.B = B
        
        
        if loss == None and self.loss != None:
            loss = self.loss
        if loss == None and self.loss == None:            
            loss = 0
            print ("WARNING: pypho_optfi: No filter loss specified! So I'm using ", loss, " dB")                
            
        self.loss = loss
        
        
        
            
        if plotfilter == None and self.plotfilter != None:            
            plotfilter = self.plotfilter
                    
        if plotfilter == None and self.plotfilter == None:                        
            plotfilter = False
    
        self.plotfilter = plotfilter         
        

########################################################################
