##!/usr/bin/env python3
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_fiber import pypho_fiber
from pypho_optfi import pypho_optfi
from pypho_functions import *
import numpy as np
import copy
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
hn = 4.
plt.close('all')

# Define network elements

# Symbolrate hier bestimmen
gp       = pypho_setup(nos = 128, sps = 128, symbolrate = 40.0e9)                      # SYMBOLRATE 

bitsrc   = pypho_symbols(glova = gp, nos = gp.nos, pattern = 'random')
esigsrc  = pypho_signalsrc(glova = gp, pulseshape = 'gauss_rz' , fwhm = 0.4*np.sqrt(2))

sig_1550 = pypho_lasmod(glova = gp, power = 0, Df = 0, teta = 0)
sig_1546 = pypho_lasmod(glova = gp, power = 0, Df = +500, teta = 0)
sig_1554 = pypho_lasmod(glova = gp, power = 0, Df = -500, teta = 0)

filter_1550 = pypho_optfi(glova = gp, Df = 0,    B = 150)
filter_1546 = pypho_optfi(glova = gp, Df = +500, B = 150)
filter_1554 = pypho_optfi(glova = gp, Df = -500, B = 150)

# Faserparameter hier definieren
SSMF     = pypho_fiber(glova = gp, l = 67.0e3,  D = 16.8,  S = 0.058, alpha = 0.2e-12, gamma = 0, phi_max=10.0)
LEAF     = pypho_fiber(glova = gp, l = 93.0e3,  D = 4.2,   S = 0.086, alpha = 0.2e-12, gamma = 0, phi_max=10.0)
DCF      = pypho_fiber(glova = gp, l = 10,  D = -120.,  S = 0, alpha = 0.2e-12, gamma = 0, phi_max=10.0)

# Simulation
bits = bitsrc()                                     # Bitsequence erzeugen

esig = esigsrc(bitsequence = bits)                  # Elektrisches Signal                 
E_1550 = sig_1550(esig = esig)                   # Optisches Signal      
E_1546 = sig_1546(esig = esig)                   # Optisches Signal      
E_1554 = sig_1554(esig = esig)                   # Optisches Signal      


plt.figure(1)

plt.subplot(3, 1, 1)
plt.plot(gp.timeax()[0:gp.sps],np.rot90( np.reshape(np.abs(E_1546[0]['E'][0])**hn, (gp.nos, gp.sps)) ), 'r')
plt.grid(True)
plt.ylabel('Optical power in W')
plt.title('1546 nm')

plt.subplot(3, 1, 2)
plt.plot(gp.timeax()[0:gp.sps],np.rot90( np.reshape(np.abs(E_1550[0]['E'][0])**hn, (gp.nos, gp.sps)) ), 'r')
plt.grid(True)
plt.ylabel('Optical power in W')
plt.title('1550 nm')

plt.subplot(3, 1, 3)
plt.plot(gp.timeax()[0:gp.sps],np.rot90( np.reshape(np.abs(E_1554[0]['E'][0])**hn, (gp.nos, gp.sps)) ), 'r')
plt.grid(True)


E = copy.deepcopy(E_1550)
E[0]['E'][0] = E_1550[0]['E'][0] + 1*E_1546[0]['E'][0] + E_1554[0]['E'][0] # Multiplex all signals 

# Fiber transmission
N_span = 1
for n_span in range(N_span):
    E = SSMF(E = E)                                                                     # Signal ueber LWL
    E = LEAF(E = E)                                                                     # Signal ueber LWL

E = DCF(E = E)             # Hier die DCF "aktivieren"


# Filter out the channels
E_1550 = filter_1550(E = copy.deepcopy(E))
E_1546 = filter_1546(E = copy.deepcopy(E))
E_1554 = filter_1554(E = copy.deepcopy(E))



# Plot Input and Output signal 
plt.figure(1)

plt.subplot(3, 1, 1)
plt.plot(gp.timeax()[0:gp.sps],np.rot90( np.reshape(np.abs(E_1546[0]['E'][0])**hn, (gp.nos, gp.sps)) ), 'b')
plt.grid(True)
plt.ylabel('Optical power in W')
plt.title('1546 nm')

plt.subplot(3, 1, 2)
plt.plot(gp.timeax()[0:gp.sps],np.rot90( np.reshape(np.abs(E_1550[0]['E'][0])**hn, (gp.nos, gp.sps)) ), 'b')
plt.grid(True)
plt.ylabel('Optical power in W')
plt.title('1550 nm')

plt.subplot(3, 1, 3)
plt.plot(gp.timeax()[0:gp.sps],np.rot90( np.reshape(np.abs(E_1554[0]['E'][0])**hn, (gp.nos, gp.sps)) ), 'b')
plt.grid(True)
#plt.axvspan(7/gp.symbolrate*1.0e12, 8/gp.symbolrate*1.0e12, facecolor='g', alpha=0.5)
plt.xlabel('Time in seconds')
plt.title('1554 nm')
plt.ylabel('Optical power in W')

plt.show()
