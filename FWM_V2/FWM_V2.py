#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_fiber import pypho_fiber
from pypho_cwlaser import pypho_cwlaser
from pypho_optfi import pypho_optfi
from pypho_oamp import pypho_oamp
from pypho_functions import *
import numpy as np
import copy
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt


plt.close('all')

# Define network elements
gp       = pypho_setup(nos = 128, sps = 64, symbolrate = 10e9)
sig_f0   = pypho_cwlaser(glova = gp, power = 0, Df = 0,  teta =0*np.pi/4.0)
ofilter = pypho_optfi(glova = gp, Df = 0, B = 20, filtype='cosrolloff', alpha = 1)
HNLF     = pypho_fiber(glova = gp, l = 5e3,  D = 0.00,   S = 0.0, alpha = 0.2, gamma = 1.4, phi_max = .01)
SSMF     = pypho_fiber(glova = gp, l = 200e3,  D = 0.00,   S = 0.0, alpha = 0.2e-12, gamma = 1.4e-12, phi_max = .1)
bitsrc   = pypho_symbols(glova = gp, nos = gp.nos, pattern = 'random')
esigsrc  = pypho_signalsrc(glova = gp, pulseshape = 'gauss_rz' , fwhm = 0.33)
sig      = pypho_lasmod(glova = gp, power = -10, Df = 50, teta = 0/8*np.pi)        # Set Polarisation here
oamp     = pypho_oamp(glova = gp)
# Simulation

# Define wavelength channels

E_f0    = sig_f0()                                              

bits    = bitsrc()
esig    = esigsrc(bitsequence = bits)
E_sig   = sig(esig = esig)                                              
E_sig   = ofilter(E=E_sig, Df = sig.Df[0])

E_sig0    = copy.deepcopy(E_sig)


### First transmission fiber
E_sig = SSMF(E = E_sig)    
E_sig1    = copy.deepcopy(E_sig)


E_bWC    = copy.deepcopy(E_sig)  
E_aWC    = copy.deepcopy(E_sig)  
E_sig2   = copy.deepcopy(E_sig)

### Wavelength converter
'''
E       = copy.deepcopy(E_sig1)
# Add FWM-pump
E[0]['E'][0] = E_f0[0]['E'][0] + E_sig[0]['E'][0] ; E[0]['E'][1] = E_f0[0]['E'][1] + E_sig[0]['E'][1]  # Multiplex all signals X-& Y-Pol 

E_bWC    = copy.deepcopy(E)  
# Transmit over HNLF
E = HNLF(E = E)    
E_aWC    = copy.deepcopy(E)

# Filter out data signal
E_sig = ofilter(E = E, Df=- sig.Df[0])
E_sig2    = copy.deepcopy(E_sig)
'''


# Second transmission fiber
E_sig = SSMF(E = E_sig)   
E_sig3    = copy.deepcopy(E_sig)



# Plot Spectrum Input signals
plt.figure(1)
ax1 = plt.subplot(2, 1, 1)
plt.semilogy((gp.freqax()-gp.f0)*1e-9, (abs(fftshift(fft(E_bWC[0]['E'][0]   )**2))), 'r', label='Before WC')

plt.title("Input spectrum", loc='left')
plt.ylabel('Spec. density');
ax_tmp = np.abs(gp.freqax()[0]-gp.f0)*1e-9//(np.abs(sig.Df[0]-sig_f0.Df[0]))
ax_tmp *=  np.abs(sig.Df[0]-sig_f0.Df[0])
plt.xticks(np.arange( -ax_tmp, ax_tmp, np.abs(sig.Df[0]-sig_f0.Df[0])))
plt.grid(True)
plt.legend()

# Plot Spectrum Output signals
ax2 = plt.subplot(2, 1, 2, sharex=ax1)
plt.semilogy((gp.freqax()-gp.f0)*1e-9, (abs(fftshift(fft(E_aWC[0]['E'][0]   )**2))), 'r', label='After WC')
plt.title("Output spectrum", loc='left')
plt.ylabel('Spec. density'); plt.xlabel('Frequency deviation [GHz]');
plt.grid(True)




plt.figure(2)
plt.ylabel('$|E|^2$'); plt.xlabel('Time [ps]');
plt.plot(gp.timeax()*1.0e12, np.abs(E_sig0[0]['E'][0])**2 / np.mean(np.abs(E_sig0[0]['E'][0])**2), 'r', label='Tx')
plt.plot(gp.timeax()*1.0e12, np.abs(E_sig1[0]['E'][0])**2 / np.mean(np.abs(E_sig1[0]['E'][0])**2), 'g', label='After 80km')
plt.plot(gp.timeax()*1.0e12, np.abs(E_sig2[0]['E'][0])**2 / np.mean(np.abs(E_sig2[0]['E'][0])**2), 'b', label='After WC')
plt.plot(gp.timeax()*1.0e12, np.abs(E_sig3[0]['E'][0])**2 / np.mean(np.abs(E_sig3[0]['E'][0])**2), 'k', label='After 2x80km')
legend = plt.legend(loc='upper right')
plt.grid(True)


plt.figure(3)
plt.subplot(4, 1, 1);plt.ylabel('$|E|^2$');plt.title('Tx')
plt.plot(np.reshape(np.abs(E_sig0[0]['E'][0])**2,(gp.nos, gp.sps)).transpose() , 'r')
plt.subplot(4, 1, 2);plt.ylabel('$|E|^2$');plt.title('After 80km')
plt.plot(np.reshape(np.abs(E_sig1[0]['E'][0])**2,(gp.nos, gp.sps)).transpose() , 'g')
plt.subplot(4, 1, 3);plt.ylabel('$|E|^2$');plt.title('After WC')
plt.plot(np.reshape(np.abs(E_sig2[0]['E'][0])**2,(gp.nos, gp.sps)).transpose() , 'b')
plt.subplot(4, 1, 4);plt.ylabel('$|E|^2$');plt.title('After 2x80km')
plt.plot(np.reshape(np.abs(E_sig3[0]['E'][0])**2,(gp.nos, gp.sps)).transpose() , 'k')

legend = plt.legend(loc='upper right')

plt.show()
