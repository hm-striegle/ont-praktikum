##!/usr/bin/env python2
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_fiber import pypho_fiber
from pypho_oamp import pypho_oamp
from pypho_functions import *
import numpy as np
import copy
import matplotlib.pyplot as plt


plt.close('all')

# Define network elements
gp       = pypho_setup(nos = 16, sps = 64, symbolrate = 10e9)
bitsrc   = pypho_symbols(glova = gp, nos = gp.nos, pattern = 'ones')
esigsrc  = pypho_signalsrc(glova = gp, pulseshape = 'gauss_rz' , fwhm = 0.33)
sig      = pypho_lasmod(glova = gp, power = 0, Df = 0, teta = 0/8*np.pi)        # Set Polarisation here
oamp     = pypho_oamp(glova = gp)
SSMF     = pypho_fiber(glova = gp, l = 80e3,  D = 2.0,   S = 0.0, alpha = 0.2, gamma = 1.4, phi_max = .01) # Set fiber parameter here
DCF      = pypho_fiber(glova = gp, l = 1,  D = -SSMF.l*SSMF.D,   S = 0.0, alpha = 0.2e-12, gamma = 1e-12, phi_max = .1) # Set fiber parameter here1

# Simulation

# Define pulses
bits    = bitsrc()*0

#bits[0:32] = 1
bits[8:10] = 1
#bits[8] = 1
#bits[10] = 1

esig    = esigsrc(bitsequence = bits)
E       = sig(esig = esig)                                              

P = 1-6
E = oamp(E=E, Pmean = P) 
E_Tx = copy.deepcopy(E)



N = 10

# Transmission
for c in range(N):     
    E = SSMF(E = E)    
    E = oamp(E=E, G = SSMF.l*0.2e-3)

# Compensation    
for c in range(N):    
    E = DCF(E = E) 
    
# Transmission    
for c in range(N):     
    E = SSMF(E = E)    
    E = oamp(E=E, G = SSMF.l*0.2e-3)    

    
# Compensation    
for c in range(N):    
    E = DCF(E = E)     


# Plot power and phase of both pol axis
plt.figure(1)
plt.plot(gp.timeax()*1.0e12, np.abs(E_Tx[0]['E'][0])**2 * 1e3, 'r', label='$E_x(0, t)$')
plt.plot(gp.timeax()*1.0e12, np.abs(E[0]['E'][0]   )**2 * 1e3, 'g', label='$E_x(z, t)$')
plt.ylabel('$10log |E_{x,y}|^2$ in mW'); plt.xlabel('Time [ps]');
plt.grid(True)
plt.legend()

