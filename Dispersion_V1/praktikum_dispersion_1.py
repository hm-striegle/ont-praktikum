##!/usr/bin/env python3
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_fiber import pypho_fiber
from pypho_functions import *
import numpy as np
import copy
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
hn = 2.
plt.close('all')

# Define network elements

# Define symbolrate here
gp       = pypho_setup(nos = 16, sps = 256, symbolrate = 10.0e9)                      # SYMBOLRATE 

bitsrc   = pypho_symbols(glova = gp, nos = gp.nos, pattern = 'singlepulse')
esigsrc  = pypho_signalsrc(glova = gp, pulseshape = 'gauss_rz' , fwhm = 0.4*np.sqrt(2))
sig_1550 = pypho_lasmod(glova = gp, power = 0, Df = 0, teta = 0)

# Define fiber parameter HERE
SSMF     = pypho_fiber(glova = gp, l = 1.0e3,  D = 3020,   S = 0, alpha = 0.2e-12, gamma = 0, phi_max=10.0)

# Simulation
bits = bitsrc()                                                                     # Bitsequence erzeugen

  
esig = esigsrc(bitsequence = bits)                                                  # Electrical signal
E_Tx = sig_1550(esig = esig)                                                        # Optical signal

E_Tx[0]['E'][0] /= np.max(E_Tx[0]['E'][0])                                          # Normalize
E = copy.deepcopy(E_Tx)

# Fiber trannsmission
E = SSMF(E = E)                                                           # Signal ueber LWL


# Plot Input and Output signal 

plt.figure(1)
plt.plot(gp.timeax()*1.0e12, np.abs(E_Tx[0]['E'][0])**hn, 'r', label='$z=0$ km')
plt.plot(gp.timeax()*1.0e12, np.abs(   E[0]['E'][0])**hn, 'g', label='$z='+ str(SSMF.l/1e3)+ '$ km')
plt.axvspan(7/gp.symbolrate*1.0e12, 8/gp.symbolrate*1.0e12, facecolor='g', alpha=0.5)
plt.axvspan(8/gp.symbolrate*1.0e12, 9/gp.symbolrate*1.0e12, facecolor='r', alpha=0.5)
plt.axvspan(9/gp.symbolrate*1.0e12, 10/gp.symbolrate*1.0e12, facecolor='g', alpha=0.5)


# Get FWHM of the input signal E_Tx

try:
    spline = UnivariateSpline(gp.timeax()*1.0e12, np.abs(E_Tx[0]['E'][0])**hn-np.max(np.abs(E_Tx[0]['E'][0])**hn)/2.0, s=0)
    r1, r2 = spline.roots() # find the roots

    plt.annotate(s='', xy=(r1,np.max(np.abs(E_Tx[0]['E'][0])**hn)/2.0), xytext=(r2,np.max(np.abs(E_Tx[0]['E'][0])**hn)/2.0), arrowprops=dict(arrowstyle='<->'))
    plt.text(r1+(r2-r1)/2.0, 0.01 +np.max(np.abs(E_Tx[0]['E'][0])**hn)/2, '$T_{FWHM,0}$ = ' + str(np.round(r2-r1,2)) + ' ps', fontsize=12, horizontalalignment='center')
    plt.axvspan(r1, r2, facecolor='g', alpha=0.5)
    T_FWHM_0 = (r2-r1) * 1e-12
    T_0_plot = T_FWHM_0 / 2.35482
    print("T_FWHM_0 = ", T_FWHM_0*1e12, " ps")


    # Get FWHM of the output signal E
    spline = UnivariateSpline(gp.timeax()*1.0e12, np.abs(E[0]['E'][0])**hn-np.max(np.abs(E[0]['E'][0])**hn)/2.0, s=0)
    r1, r2 = spline.roots() # find the roots

    plt.annotate(s='', xy=(r1,np.max(np.abs(E[0]['E'][0])**hn)/2.0), xytext=(r2,np.max(np.abs(E[0]['E'][0])**hn)/2), arrowprops=dict(arrowstyle='<->'))
    plt.text(r1+(r2-r1)/2.0, 0.01 + np.max(np.abs(E[0]['E'][0])**hn)/2, '$T_{FWHM,1}$ = ' + str(np.round(r2-r1,2)) + ' ps', fontsize=12, horizontalalignment='center')
    plt.ylabel('$|E|^'+str(hn)+'$ a.u.'); plt.xlabel('Time $t$ [ps]'); legend = plt.legend(loc='upper right')

    T_FWHM_1 = (r2-r1) * 1e-12
    
    print("T_FWHM_1 = ", T_FWHM_1*1e12, " ps")

    
    
except: 
        print("Pulse width not determinable!")

    

plt.show()
