##!/usr/bin/env python3
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_fiber import pypho_fiber
from pypho_functions import *
import numpy as np
import copy
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
hn = 2.
plt.close('all')

# Define network elements

# Define the symbolrate here :)
gp       = pypho_setup(nos = 64, sps = 64, symbolrate = 10.0e9)                      # SYMBOLRATE 

bitsrc   = pypho_symbols(glova = gp, nos = gp.nos, pattern = 'debruijn')
esigsrc  = pypho_signalsrc(glova = gp, pulseshape = 'gauss_rz' , fwhm = 0.45*np.sqrt(2))
sig_1550 = pypho_lasmod(glova = gp, power = 0, Df = 0, teta = 0)

# Define the fiber parameter here
SSMF     = pypho_fiber(glova = gp, l = 150.0e3,  D = 16.8,  S = 0, alpha = 0.2e-12, gamma = 0, phi_max=10.0)
DCF      = pypho_fiber(glova = gp, l = 1.0e3,   D = -120., S = 0, alpha = 0.2e-12, gamma = 0, phi_max=10.0)

# Simulation
bits = bitsrc()                                                                     # Bitsequence erzeugen


esig = esigsrc(bitsequence = bits)                                                  # Elektrisches Signal erzeugen
E_Tx = sig_1550(esig = esig)                                                        # Optisches Signal erzeugen

E_Tx[0]['E'][0] /= np.max(E_Tx[0]['E'][0])                                          # Auf maximale Leistung normieren
E = copy.deepcopy(E_Tx)

# Fiber transmission
N_span = 1
for n_span in range(N_span):
    E = SSMF(E = E)                                                                     # Signal ueber LWL

#E = DCF(E = E)             # Activate DCF here




# Plot Input and Output signal 
plt.figure(1)

plt.plot(gp.timeax()[0:gp.sps],np.rot90( np.reshape(np.abs(E_Tx[0]['E'][0])**hn, (gp.nos, gp.sps)) ), 'b', linewidth=2, alpha= .01)
plt.plot(gp.timeax()[0:gp.sps]-gp.timeax()[gp.sps-1],np.rot90( np.reshape(np.roll(np.abs(E_Tx[0]['E'][0])**hn, 1), (gp.nos, gp.sps)) ), 'b', linewidth=2, alpha= .01)
plt.plot(gp.timeax()[0:gp.sps]+gp.timeax()[gp.sps-1],np.rot90( np.reshape(np.roll(np.abs(E_Tx[0]['E'][0])**hn,-1), (gp.nos, gp.sps)) ), 'b', linewidth=2, alpha= .01)

colco = np.array(["r", "g"])

for c in range(0,gp.nos):

    plt.plot(gp.timeax()[0:gp.sps],                       np.abs(E[0]['E'][0][c*gp.sps:(c+1)*gp.sps])**hn, colco[int(bits[c])])
    plt.plot(gp.timeax()[0:gp.sps]-gp.timeax()[gp.sps-1], np.abs(E[0]['E'][0][c*gp.sps:(c+1)*gp.sps])**hn, colco[int(bits[c])])
    plt.plot(gp.timeax()[0:gp.sps]+gp.timeax()[gp.sps-1], np.abs(E[0]['E'][0][c*gp.sps:(c+1)*gp.sps])**hn, colco[int(bits[c])])


plt.xlabel('Time in seconds')
plt.ylabel('Optical power in W')
plt.grid(True)
plt.show()
