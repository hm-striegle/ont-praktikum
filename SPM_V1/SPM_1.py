##!/usr/bin/env python2
# -*- coding: utf-8 -*-

#Import functions and libraries
import sys
sys.path.append('../')
from pypho_setup import pypho_setup
from pypho_symbols import pypho_symbols
from pypho_signalsrc import pypho_signalsrc
from pypho_lasmod import pypho_lasmod
from pypho_fiber import pypho_fiber
from pypho_oamp import pypho_oamp
from pypho_functions import *
import numpy as np
import copy
import matplotlib.pyplot as plt


plt.close('all')

# Define network elements
gp       = pypho_setup(nos = 8, sps = 1*128, symbolrate = 10e9)
bitsrc   = pypho_symbols(glova = gp, nos = gp.nos, pattern = 'ones')
esigsrc  = pypho_signalsrc(glova = gp, pulseshape = 'gauss_rz' , fwhm = 0.33)
sig      = pypho_lasmod(glova = gp, power = 0, Df = 0, teta = 0/8*np.pi)        # Set Polarisation here
oamp     = pypho_oamp(glova = gp)
SSMF     = pypho_fiber(glova = gp, l = 10e3,  D = 0.0,   S = 0.0, alpha = 0.2, gamma = 1.4, phi_max = .01) # Set fiber parameter here

# Simulation

# Define wavelength channel
bits    = bitsrc()
esig    = esigsrc(bitsequence = bits)
E       = sig(esig = esig)                                              



# Set mean signal pwer in! In dBm
E = oamp(E=E, Pmean = 0)  

E_Tx = copy.deepcopy(E)

# Fiber transmission
     
E = SSMF(E = E)    

# Plot power and phase of both pol axis
plt.figure(1)
plt.subplot(3, 1, 1)
plt.plot(gp.timeax()*1.0e12, np.abs(E[0]['E'][0])**2*1e3, 'r', label='$E_x(0, t)$')
plt.plot(gp.timeax()*1.0e12, np.abs(E[0]['E'][1])**2*1e3, 'g', label='$E_y(0, t)$')
plt.ylabel('$10log |E_{x,y}|^2$ in mW'); plt.xlabel('Time [ps]');
plt.grid(True)
plt.legend()

plt.subplot(3, 1, 2)
plt.plot(gp.timeax()*1.0e12, np.angle(E[0]['E'][0]), 'r')
plt.plot(gp.timeax()*1.0e12, np.angle(E[0]['E'][1]), 'g')
plt.ylabel('$ \phi_{x,y} $ in rad'); plt.xlabel('Time [ps]');
plt.grid(True)
plt.show()

plt.subplot(3, 1, 3)
plt.plot(gp.timeax()*1.0e12, 1/(2*np.pi)*np.gradient(np.angle(E[0]['E'][0]))*1e-9, 'r')
plt.plot(gp.timeax()*1.0e12, 1/(2*np.pi)*np.gradient(np.angle(E[0]['E'][1]))*1e-9, 'g')
plt.ylabel('$ \Delta f_{x,y} $ in GHz'); plt.xlabel('Time [ps]');
plt.grid(True)
plt.show()

plt.figure(2)
plt.subplot(2, 1, 1)
plt.semilogy(gp.freqax(), np.abs( fftshift(fft(E[0]['E'][0])))**2, 'g', label='$E_{x,out}$')
plt.semilogy(gp.freqax(), np.abs( fftshift(fft(E_Tx[0]['E'][0])))**2, 'r', label='$E_{x,in}$')
plt.ylabel('Spectral power density'); plt.xlabel('Frequency [THz]');
plt.grid(True)
plt.legend()
plt.subplot(2, 1, 2)
plt.semilogy(gp.freqax(), np.abs( fftshift(fft(E[0]['E'][1])))**2, 'g', label='$E_{y,out}$')
plt.semilogy(gp.freqax(), np.abs( fftshift(fft(E_Tx[0]['E'][1])))**2, 'r', label='$E_{y,in}$')
plt.ylabel('Spectral power density'); plt.xlabel('Frequency [THz]');
plt.grid(True)
plt.legend()
